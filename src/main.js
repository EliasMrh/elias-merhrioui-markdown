/* 
Import des différents lib
*/
  // Vue
  import Vue from 'vue';

  // Inner
  import App from './App.vue';
  
  //=> Import app router
  import router from "./router";

  //=> Import app store
  import store from './store/index';




/* 
Set configuration
*/
  Vue.config.productionTip = false;
//


/* 
Create app
*/
  new Vue({   
    //=> Include app router
    router,
    
    //=> Include app store
    store,
    
    render: h => h(App),
  })
  .$mount('#app');
//